import Vue from 'vue'
import Vuetify from 'vuetify'
import '@mdi/font/css/materialdesignicons.css'
import '@fortawesome/fontawesome-free/css/all.css'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(Vuetify, {
  theme: {
    primary: '#001f3f',
    secondary: '#39cccc',
    accent: '#f012be',
    success: '#2ecc40',
    info: '#7fdbff',
    warning: '#ffdc00',
    error: '#85144b'
  }
})
