// store/facts.js
// Vuex store for MSRC facts

export const state = () => ({
  facts: [],
  fact: {}
})