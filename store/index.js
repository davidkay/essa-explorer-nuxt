// store/index.js
// Vuex store for geojson features

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const store = () => new Vuex.Store({
  state: {
    features: [],
    feature: {}
  },
  getters: {
    allFeatures (state) {
      return state.features
    },
    activeFeatures (state) {
      return state.features.filter(feature => !feature.completed)
    },
    completedFeatures (state) {
      return state.features.filter(feature => feature.completed)
    },
    completedFeaturesCount: (state, getters) => {
      return getters.completedFeatures.length
    },
    featuresCount: (state, getters) => {
      return getters.allFeatures.length
    }
  },
  mutations: {
    SET_FEATURES (state, features) {
      state.features = features.features
    },
    ADD_FEATURE (state, feature) {
      state.features.push(feature)
    },
    REMOVE_FEATURE (state, feature) {
      var i = state.features.indexOf(feature)
      state.features.splice(i, 1)
    },
    FILTER_FEATURES (state, value) {
      state.features.forEach((feature) => {
        feature.completed = !value
      })
    }
  },
  actions: {
    addFeature ({ commit }, feature) {
      commit('ADD_FEATURE', feature)
    },
    setFeatures ({ commit }, features) {
      commit('SET_FEATURES', features)
    },
    removeFeature ({ commit }, feature) {
      commit('REMOVE_FEATURE', feature)
    },
    allDone ({ state, commit }) {
      var value = state.features.filter(feature => feature.completed).length === state.features.length
      commit('FILTER_FEATURES', value)
    },
    saveFeatures ({ state }) {
      // axios.put('/api/features', { features: state.features })
    },
    async browserInit ({commit}) {
      let {data} = await axios.get('https://cdn.jsdelivr.net/gh/davidbkay/mde-open-data@0.0.2/mississippi-districts-1516.geojson')
      commit('SET_FEATURES', data)
    }
  }
})

export default store
