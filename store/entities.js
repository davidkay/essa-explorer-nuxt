// store/entities.js
// Vuex store for MSRC entities

export const state = () => ({
  entities: [],
  entity: {}
})